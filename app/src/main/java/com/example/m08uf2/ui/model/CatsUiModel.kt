package com.example.m08uf2.ui.model

data class CatsUiModel (
    val id: String,
    val name: String,
    val temperament: String,
    val countryCode: String,
    val description: String,
    val wikipedia_url: String,
    val image_url: String
)