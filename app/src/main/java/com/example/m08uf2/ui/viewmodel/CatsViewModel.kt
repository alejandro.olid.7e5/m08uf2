package com.example.m08uf2.ui.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.m08uf2.data.apiservice.CatsApi
import com.example.m08uf2.ui.model.CatsUiModel
import com.example.m08uf2.ui.model.mapper.CatsDtoUiModelMapper
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel() {

    var catsUiState: List<CatsUiModel> by mutableStateOf(emptyList())
        private set

    var mapper = CatsDtoUiModelMapper()

    init {
        getCats()
    }

    fun getCats() {
        viewModelScope.launch {
            val catsListDto = CatsApi.retrofitService.getCatsRx()
            val imagesListDto = catsListDto.map {CatsApi.retrofitService.getCatImageRx(it.id).firstOrNull() }
            catsUiState = mapper.map(catsListDto, imagesListDto)
        }
    }
}