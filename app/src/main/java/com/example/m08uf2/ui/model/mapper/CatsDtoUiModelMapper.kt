package com.example.m08uf2.ui.model.mapper

import com.example.m08uf2.data.apiservice.model.Cats
import com.example.m08uf2.data.apiservice.model.CatsImages
import com.example.m08uf2.ui.model.CatsUiModel

class CatsDtoUiModelMapper {
    fun map(cats: List<Cats>, catsImages: List<CatsImages?>): List<CatsUiModel> {
        return mutableListOf<CatsUiModel>().apply {
            for (i in cats.indices)
                add(
                    CatsUiModel(
                        cats[i].id, cats[i].name, cats[i].temperament,
                        cats[i].countryCode, cats[i].description, cats[i].wikipedia_url,
                        catsImages?.get(i)?.imageUrl ?: ""
                    )
                )
        }
    }
}