package com.example.m08uf2.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatsImages(
    @SerialName("url") val imageUrl: String = ""
)