package com.example.m08uf2.data.apiservice

import com.example.m08uf2.data.apiservice.model.Cats
import com.example.m08uf2.data.apiservice.model.CatsImages
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL =
    "https://api.thecatapi.com/v1/"

private const val API_KEY =
    "live_7zkU8PCRIJnT6evxGny9PfRCPOzQD6jhM1aBjIbRj5Gnb7tniLqRJLQIGgxo69Cp"

private val jsonIgnored = Json { ignoreUnknownKeys = true }

private val retrofit = Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()

interface CatsApiService {
    @GET("breeds")
    suspend fun getCatsRx(): List<Cats>

    @GET("images/search")
    suspend fun getCatImageRx(@Query("breed_id") id: String): List<CatsImages>
}

object CatsApi {
    val retrofitService : CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}